# CubeLab ECAD Files

This project contains all the revisions of the CubeLab PCBs, as well as the schematics and BOMs for each. The current
revision in Rev10, and can be found in its own folder.

We currently use the OshPark PCB design rules, which can be found [here](https://docs.oshpark.com/design-tools/kicad/).

You will need KiCAD to open these designs. You can find the latest version of KiCAD [here](https://www.kicad.org/).
On Ubuntu, you can install KiCAD with `sudo apt install kicad`.

### Authors:

Galen Nare

---

### License:

All Rights Reserved (sorry).

These designs are the property of the University of Delaware Department of Mechanical Engineering. 
Please do not use or distribute without permission. 