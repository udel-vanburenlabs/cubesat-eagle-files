<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="t3D" color="7" fill="5" visible="no" active="no"/>
<layer number="58" name="b3D" color="7" fill="4" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="rpi-pico">
<packages>
<package name="SC0915_RPI">
<smd name="1" x="-9.6901" y="24.13" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="2" x="-9.6901" y="21.59" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="3" x="-9.6901" y="19.05" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="4" x="-9.6901" y="16.51" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="5" x="-9.6901" y="13.97" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="6" x="-9.6901" y="11.43" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="7" x="-9.6901" y="8.89" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="8" x="-9.6901" y="6.35" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="9" x="-9.6901" y="3.81" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="10" x="-9.6901" y="1.27" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="11" x="-9.6901" y="-1.27" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="12" x="-9.6901" y="-3.81" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="13" x="-9.6901" y="-6.35" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="14" x="-9.6901" y="-8.89" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="15" x="-9.6901" y="-11.43" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="16" x="-9.6901" y="-13.97" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="17" x="-9.6901" y="-16.51" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="18" x="-9.6901" y="-19.05" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="19" x="-9.6901" y="-21.59" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="20" x="-9.6901" y="-24.13" dx="3.2004" dy="1.6002" layer="1"/>
<smd name="21" x="9.6901" y="-24.13" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="22" x="9.6901" y="-21.59" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="23" x="9.6901" y="-19.05" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="24" x="9.6901" y="-16.51" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="25" x="9.6901" y="-13.97" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="26" x="9.6901" y="-11.43" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="27" x="9.6901" y="-8.89" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="28" x="9.6901" y="-6.35" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="29" x="9.6901" y="-3.81" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="30" x="9.6901" y="-1.27" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="31" x="9.6901" y="1.27" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="32" x="9.6901" y="3.81" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="33" x="9.6901" y="6.35" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="34" x="9.6901" y="8.89" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="35" x="9.6901" y="11.43" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="36" x="9.6901" y="13.97" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="37" x="9.6901" y="16.51" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="38" x="9.6901" y="19.05" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="39" x="9.6901" y="21.59" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="40" x="9.6901" y="24.13" dx="3.2004" dy="1.6002" layer="1" rot="R180"/>
<smd name="TP2" x="1.000759375" y="24.30018125" dx="1.4986" dy="1.4986" layer="1"/>
<smd name="TP3" x="-1.000759375" y="24.30018125" dx="1.4986" dy="1.4986" layer="1"/>
<smd name="TP4" x="-2.499359375" y="17.5006" dx="1.4986" dy="1.4986" layer="1"/>
<smd name="TP5" x="-2.499359375" y="15.001240625" dx="1.4986" dy="1.4986" layer="1"/>
<smd name="TP6" x="-2.499359375" y="12.50188125" dx="1.4986" dy="1.4986" layer="1"/>
<smd name="A" x="-2.72541875" y="24.000459375" dx="1.0922" dy="1.8034" layer="1"/>
<smd name="B" x="2.72541875" y="24.000459375" dx="1.0922" dy="1.8034" layer="1"/>
<smd name="C" x="-2.4257" y="20.970240625" dx="1.0414" dy="1.4478" layer="1"/>
<smd name="D" x="2.4257" y="20.970240625" dx="1.0414" dy="1.4478" layer="1"/>
<smd name="D2" x="0" y="-24.698959375" dx="3.2004" dy="1.6002" layer="1" rot="R90"/>
<smd name="D1" x="-2.54" y="-24.698959375" dx="3.2004" dy="1.6002" layer="1" rot="R90"/>
<smd name="D3" x="2.54" y="-24.698959375" dx="3.2004" dy="1.6002" layer="1" rot="R90"/>
<smd name="TP1" x="0" y="21.00071875" dx="1.4986" dy="1.4986" layer="1"/>
<wire x1="10.6172" y1="0.127" x2="10.6172" y2="-0.127" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="2.667" x2="10.6172" y2="2.413" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="5.207" x2="10.6172" y2="4.953" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="7.747" x2="10.6172" y2="7.493" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="10.287" x2="10.6172" y2="10.033" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="12.827" x2="10.6172" y2="12.573" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="15.367" x2="10.6172" y2="15.113" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="17.907" x2="10.6172" y2="17.653" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="20.447" x2="10.6172" y2="20.193" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="22.987" x2="10.6172" y2="22.733" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-25.6286" x2="-1.397" y2="-25.6286" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="-25.6286" x2="-10.6172" y2="-25.6286" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-25.6286" x2="1.143" y2="-25.6286" width="0.1524" layer="21"/>
<wire x1="-4.1148" y1="25.6286" x2="-4.1148" y2="26.924" width="0.1524" layer="21"/>
<wire x1="-4.1148" y1="26.924" x2="4.1148" y2="26.924" width="0.1524" layer="21"/>
<wire x1="4.1148" y1="26.924" x2="4.1148" y2="25.6286" width="0.1524" layer="21"/>
<wire x1="4.1148" y1="25.6286" x2="10.6172" y2="25.6286" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="25.6286" x2="10.6172" y2="25.273" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-25.6286" x2="3.683" y2="-25.6286" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-25.6286" x2="-10.6172" y2="-25.273" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="25.6286" x2="-4.1148" y2="25.6286" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="25.273" x2="-10.6172" y2="25.6286" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="22.733" x2="-10.6172" y2="22.987" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="20.193" x2="-10.6172" y2="20.447" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="17.653" x2="-10.6172" y2="17.907" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="15.113" x2="-10.6172" y2="15.367" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="12.573" x2="-10.6172" y2="12.827" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="10.033" x2="-10.6172" y2="10.287" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="7.493" x2="-10.6172" y2="7.747" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="4.953" x2="-10.6172" y2="5.207" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="2.413" x2="-10.6172" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-0.127" x2="-10.6172" y2="0.127" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-2.667" x2="-10.6172" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-5.207" x2="-10.6172" y2="-4.953" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-7.747" x2="-10.6172" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-10.287" x2="-10.6172" y2="-10.033" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-12.827" x2="-10.6172" y2="-12.573" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-15.367" x2="-10.6172" y2="-15.113" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-17.907" x2="-10.6172" y2="-17.653" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-20.447" x2="-10.6172" y2="-20.193" width="0.1524" layer="21"/>
<wire x1="-10.6172" y1="-22.987" x2="-10.6172" y2="-22.733" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-25.273" x2="10.6172" y2="-25.6286" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-22.733" x2="10.6172" y2="-22.987" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-20.193" x2="10.6172" y2="-20.447" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-17.653" x2="10.6172" y2="-17.907" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-15.113" x2="10.6172" y2="-15.367" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-12.573" x2="10.6172" y2="-12.827" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-10.033" x2="10.6172" y2="-10.287" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-7.493" x2="10.6172" y2="-7.747" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-4.953" x2="10.6172" y2="-5.207" width="0.1524" layer="21"/>
<wire x1="10.6172" y1="-2.413" x2="10.6172" y2="-2.667" width="0.1524" layer="21"/>
<wire x1="-14.1224" y1="24.13" x2="-14.3764" y2="24.13" width="0.1524" layer="21" curve="-180"/>
<wire x1="-14.3764" y1="24.13" x2="-14.1224" y2="24.13" width="0.1524" layer="21" curve="-180"/>
<text x="-1.7272" y="-0.635" size="1.27" layer="21" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="-9.7028" y1="24.13" x2="-9.7028" y2="26.67" width="0.1524" layer="47"/>
<wire x1="-9.7028" y1="26.67" x2="-9.7028" y2="27.051" width="0.1524" layer="47"/>
<wire x1="9.7028" y1="24.13" x2="9.7028" y2="26.67" width="0.1524" layer="47"/>
<wire x1="9.7028" y1="26.67" x2="9.7028" y2="27.051" width="0.1524" layer="47"/>
<wire x1="-9.7028" y1="26.67" x2="9.7028" y2="26.67" width="0.1524" layer="47"/>
<wire x1="-9.7028" y1="26.67" x2="-9.4488" y2="26.797" width="0.1524" layer="47"/>
<wire x1="-9.7028" y1="26.67" x2="-9.4488" y2="26.543" width="0.1524" layer="47"/>
<wire x1="-9.4488" y1="26.797" x2="-9.4488" y2="26.543" width="0.1524" layer="47"/>
<wire x1="9.7028" y1="26.67" x2="9.4488" y2="26.797" width="0.1524" layer="47"/>
<wire x1="9.7028" y1="26.67" x2="9.4488" y2="26.543" width="0.1524" layer="47"/>
<wire x1="9.4488" y1="26.797" x2="9.4488" y2="26.543" width="0.1524" layer="47"/>
<wire x1="-9.7028" y1="24.13" x2="-12.2428" y2="24.13" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="24.13" x2="-12.5984" y2="24.13" width="0.1524" layer="47"/>
<wire x1="-9.7028" y1="21.59" x2="-12.2428" y2="21.59" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="21.59" x2="-12.5984" y2="21.59" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="24.13" x2="-12.2428" y2="25.4" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="21.59" x2="-12.2428" y2="20.32" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="24.13" x2="-12.3444" y2="24.384" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="24.13" x2="-12.0904" y2="24.384" width="0.1524" layer="47"/>
<wire x1="-12.3444" y1="24.384" x2="-12.0904" y2="24.384" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="21.59" x2="-12.3444" y2="21.336" width="0.1524" layer="47"/>
<wire x1="-12.2428" y1="21.59" x2="-12.0904" y2="21.336" width="0.1524" layer="47"/>
<wire x1="-12.3444" y1="21.336" x2="-12.0904" y2="21.336" width="0.1524" layer="47"/>
<wire x1="10.5156" y1="25.5016" x2="12.2428" y2="25.5016" width="0.1524" layer="47"/>
<wire x1="12.2428" y1="25.5016" x2="12.5984" y2="25.5016" width="0.1524" layer="47"/>
<wire x1="10.5156" y1="-25.5016" x2="12.2428" y2="-25.5016" width="0.1524" layer="47"/>
<wire x1="12.2428" y1="-25.5016" x2="12.5984" y2="-25.5016" width="0.1524" layer="47"/>
<wire x1="12.2428" y1="25.5016" x2="12.2428" y2="-25.5016" width="0.1524" layer="47"/>
<wire x1="12.2428" y1="25.5016" x2="12.0904" y2="25.2476" width="0.1524" layer="47"/>
<wire x1="12.2428" y1="25.5016" x2="12.3444" y2="25.2476" width="0.1524" layer="47"/>
<wire x1="12.0904" y1="25.2476" x2="12.3444" y2="25.2476" width="0.1524" layer="47"/>
<wire x1="12.2428" y1="-25.5016" x2="12.0904" y2="-25.2476" width="0.1524" layer="47"/>
<wire x1="12.2428" y1="-25.5016" x2="12.3444" y2="-25.2476" width="0.1524" layer="47"/>
<wire x1="12.0904" y1="-25.2476" x2="12.3444" y2="-25.2476" width="0.1524" layer="47"/>
<wire x1="-10.5156" y1="-25.5016" x2="-10.5156" y2="-28.0416" width="0.1524" layer="47"/>
<wire x1="-10.5156" y1="-28.0416" x2="-10.5156" y2="-28.4226" width="0.1524" layer="47"/>
<wire x1="10.5156" y1="-25.5016" x2="10.5156" y2="-28.0416" width="0.1524" layer="47"/>
<wire x1="10.5156" y1="-28.0416" x2="10.5156" y2="-28.4226" width="0.1524" layer="47"/>
<wire x1="-10.5156" y1="-28.0416" x2="10.5156" y2="-28.0416" width="0.1524" layer="47"/>
<wire x1="-10.5156" y1="-28.0416" x2="-10.2616" y2="-27.9146" width="0.1524" layer="47"/>
<wire x1="-10.5156" y1="-28.0416" x2="-10.2616" y2="-28.1686" width="0.1524" layer="47"/>
<wire x1="-10.2616" y1="-27.9146" x2="-10.2616" y2="-28.1686" width="0.1524" layer="47"/>
<wire x1="10.5156" y1="-28.0416" x2="10.2616" y2="-27.9146" width="0.1524" layer="47"/>
<wire x1="10.5156" y1="-28.0416" x2="10.2616" y2="-28.1686" width="0.1524" layer="47"/>
<wire x1="10.2616" y1="-27.9146" x2="10.2616" y2="-28.1686" width="0.1524" layer="47"/>
<text x="-15.7734" y="-34.6456" size="1.27" layer="47" ratio="6" rot="SR0">Default Padstyle: RX126Y63D0T</text>
<text x="-14.8082" y="-39.2176" size="1.27" layer="47" ratio="6" rot="SR0">Alt 1 Padstyle: OX60Y90D30P</text>
<text x="-14.8082" y="-40.7416" size="1.27" layer="47" ratio="6" rot="SR0">Alt 2 Padstyle: OX90Y60D30P</text>
<text x="-4.0386" y="27.178" size="0.635" layer="47" ratio="4" rot="SR0">0.763in/19.38mm</text>
<text x="-19.1008" y="22.5552" size="0.635" layer="47" ratio="4" rot="SR0">0.1in/2.54mm</text>
<text x="12.7508" y="-0.3048" size="0.635" layer="47" ratio="4" rot="SR0">2.008in/51.003mm</text>
<text x="-4.318" y="-29.1846" size="0.635" layer="47" ratio="4" rot="SR0">0.827in/21.006mm</text>
<wire x1="-4.0132" y1="26.797" x2="-4.0132" y2="25.5016" width="0.1524" layer="51"/>
<wire x1="-4.0132" y1="26.797" x2="4.0132" y2="26.797" width="0.1524" layer="51"/>
<wire x1="4.0132" y1="26.797" x2="4.0132" y2="25.5016" width="0.1524" layer="51"/>
<wire x1="-10.5156" y1="-25.5016" x2="10.5156" y2="-25.5016" width="0.1524" layer="51"/>
<wire x1="10.5156" y1="-25.5016" x2="10.5156" y2="25.5016" width="0.1524" layer="51"/>
<wire x1="10.5156" y1="25.5016" x2="0.3048" y2="25.5016" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="25.5016" x2="-0.3048" y2="25.5016" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="25.5016" x2="-10.5156" y2="25.5016" width="0.1524" layer="51"/>
<wire x1="-10.5156" y1="25.5016" x2="-10.5156" y2="-25.5016" width="0.1524" layer="51"/>
<wire x1="-8.0264" y1="24.13" x2="-8.1788" y2="24.13" width="0" layer="51" curve="-180"/>
<wire x1="-8.1788" y1="24.13" x2="-8.0264" y2="24.13" width="0" layer="51" curve="-180"/>
<wire x1="0.3048" y1="25.5016" x2="-0.3048" y2="25.5016" width="0.1524" layer="51" curve="-180"/>
<text x="-3.2766" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="SC0915">
<pin name="UART0_TX/I2C0_SDA/SPI0_RX/GP0" x="2.54" y="0" length="middle"/>
<pin name="UART0_RX/I2C0_SCL/GPI0_CSN/GP1" x="2.54" y="-2.54" length="middle"/>
<pin name="GND_2" x="2.54" y="-5.08" length="middle" direction="pas"/>
<pin name="I2C1_SDA/SPI0_SCK/GP2" x="2.54" y="-7.62" length="middle"/>
<pin name="I2C1_SCL/SPI0_TX/GP3" x="2.54" y="-10.16" length="middle"/>
<pin name="UART1_TX/I2C0_SDA/SPI0_RX/GP4" x="2.54" y="-12.7" length="middle"/>
<pin name="UART1_RX/I2C0_SCL/SPI0_CSN/GP5" x="2.54" y="-15.24" length="middle"/>
<pin name="GND_3" x="2.54" y="-17.78" length="middle" direction="pas"/>
<pin name="I2C1_SDA/SPI0_SCK/GP6" x="2.54" y="-20.32" length="middle"/>
<pin name="I2C1_SCL/SPI0_TX/GP7" x="2.54" y="-22.86" length="middle"/>
<pin name="UART1_TX/I2C0_SDA/SPI1_RX/GP8" x="2.54" y="-25.4" length="middle"/>
<pin name="UART1_RX/I2C0_SCL/SPI1_CSN/GP9" x="2.54" y="-27.94" length="middle"/>
<pin name="GND_4" x="2.54" y="-30.48" length="middle" direction="pas"/>
<pin name="I2C1_SDA/SPI1_SCK/GP10" x="2.54" y="-33.02" length="middle"/>
<pin name="I2C1_SCL/SPI1_TX/GP11" x="2.54" y="-35.56" length="middle"/>
<pin name="UART0_TX/I2C0_SDA/GPI1_RX/GP12" x="2.54" y="-38.1" length="middle"/>
<pin name="UART0_RX/I2C0_SCL/SPI1_CSN/GP13" x="2.54" y="-40.64" length="middle"/>
<pin name="GND_5" x="2.54" y="-43.18" length="middle" direction="pas"/>
<pin name="I2C1_SDA/SPI1_SCK/GP14" x="2.54" y="-45.72" length="middle"/>
<pin name="I2C1_SCL/SPI1_TX/GP15" x="2.54" y="-48.26" length="middle"/>
<pin name="UART0_TX/I2C0_SDA/SPI0_RX/GP16" x="2.54" y="-50.8" length="middle"/>
<pin name="UART0_RX/I2C0_SCL/SPI0_CSN/GP17" x="2.54" y="-53.34" length="middle"/>
<pin name="GND_6" x="2.54" y="-55.88" length="middle" direction="pas"/>
<pin name="I2C1_SDA/SPI0_SCK/GP18" x="2.54" y="-58.42" length="middle"/>
<pin name="I2C1_SCL/SPI0_TX/GP19" x="2.54" y="-60.96" length="middle"/>
<pin name="I2C0_SDA/GP20" x="2.54" y="-63.5" length="middle"/>
<pin name="I2C0_SCL/GP21" x="124.46" y="-66.04" length="middle" rot="R180"/>
<pin name="GND_7" x="124.46" y="-63.5" length="middle" direction="pas" rot="R180"/>
<pin name="GP22" x="124.46" y="-60.96" length="middle" rot="R180"/>
<pin name="RUN" x="124.46" y="-58.42" length="middle" direction="pas" rot="R180"/>
<pin name="I2C1_SDA/ADC0/GP26" x="124.46" y="-55.88" length="middle" rot="R180"/>
<pin name="I2C1_SCL/ADC1/GP27" x="124.46" y="-53.34" length="middle" rot="R180"/>
<pin name="AGND/GND" x="124.46" y="-50.8" length="middle" direction="pas" rot="R180"/>
<pin name="ADC2/GP28" x="124.46" y="-48.26" length="middle" rot="R180"/>
<pin name="ADC_VREF" x="124.46" y="-45.72" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3(OUT)" x="124.46" y="-43.18" length="middle" direction="out" rot="R180"/>
<pin name="3V3_EN" x="124.46" y="-40.64" length="middle" direction="pwr" rot="R180"/>
<pin name="GND_8" x="124.46" y="-38.1" length="middle" direction="pas" rot="R180"/>
<pin name="VSYS" x="124.46" y="-35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="VBUS" x="124.46" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="SWCLK" x="124.46" y="-30.48" length="middle" direction="pas" rot="R180"/>
<pin name="GND_9" x="124.46" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="SWDIO" x="124.46" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="GND_10" x="124.46" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="USB_DM" x="124.46" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="USB_DP" x="124.46" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="GPIO/SMPS" x="124.46" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="GPIO25/LED" x="124.46" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="BOOTSEL" x="124.46" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="GND_11" x="124.46" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="GND_12" x="124.46" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="GND_13" x="124.46" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="124.46" y="0" length="middle" direction="pas" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-71.12" x2="119.38" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="119.38" y1="-71.12" x2="119.38" y2="5.08" width="0.1524" layer="94"/>
<wire x1="119.38" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="58.7756" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="58.1406" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SC0915" prefix="U">
<gates>
<gate name="A" symbol="SC0915" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC0915_RPI">
<connects>
<connect gate="A" pin="3V3(OUT)" pad="36"/>
<connect gate="A" pin="3V3_EN" pad="37"/>
<connect gate="A" pin="ADC2/GP28" pad="34"/>
<connect gate="A" pin="ADC_VREF" pad="35"/>
<connect gate="A" pin="AGND/GND" pad="33"/>
<connect gate="A" pin="BOOTSEL" pad="TP6"/>
<connect gate="A" pin="GND" pad="D"/>
<connect gate="A" pin="GND_10" pad="TP1"/>
<connect gate="A" pin="GND_11" pad="A"/>
<connect gate="A" pin="GND_12" pad="B"/>
<connect gate="A" pin="GND_13" pad="C"/>
<connect gate="A" pin="GND_2" pad="3"/>
<connect gate="A" pin="GND_3" pad="8"/>
<connect gate="A" pin="GND_4" pad="13"/>
<connect gate="A" pin="GND_5" pad="18"/>
<connect gate="A" pin="GND_6" pad="23"/>
<connect gate="A" pin="GND_7" pad="28"/>
<connect gate="A" pin="GND_8" pad="38"/>
<connect gate="A" pin="GND_9" pad="D2"/>
<connect gate="A" pin="GP22" pad="29"/>
<connect gate="A" pin="GPIO/SMPS" pad="TP4"/>
<connect gate="A" pin="GPIO25/LED" pad="TP5"/>
<connect gate="A" pin="I2C0_SCL/GP21" pad="27"/>
<connect gate="A" pin="I2C0_SDA/GP20" pad="26"/>
<connect gate="A" pin="I2C1_SCL/ADC1/GP27" pad="32"/>
<connect gate="A" pin="I2C1_SCL/SPI0_TX/GP19" pad="25"/>
<connect gate="A" pin="I2C1_SCL/SPI0_TX/GP3" pad="5"/>
<connect gate="A" pin="I2C1_SCL/SPI0_TX/GP7" pad="10"/>
<connect gate="A" pin="I2C1_SCL/SPI1_TX/GP11" pad="15"/>
<connect gate="A" pin="I2C1_SCL/SPI1_TX/GP15" pad="20"/>
<connect gate="A" pin="I2C1_SDA/ADC0/GP26" pad="31"/>
<connect gate="A" pin="I2C1_SDA/SPI0_SCK/GP18" pad="24"/>
<connect gate="A" pin="I2C1_SDA/SPI0_SCK/GP2" pad="4"/>
<connect gate="A" pin="I2C1_SDA/SPI0_SCK/GP6" pad="9"/>
<connect gate="A" pin="I2C1_SDA/SPI1_SCK/GP10" pad="14"/>
<connect gate="A" pin="I2C1_SDA/SPI1_SCK/GP14" pad="19"/>
<connect gate="A" pin="RUN" pad="30"/>
<connect gate="A" pin="SWCLK" pad="D1"/>
<connect gate="A" pin="SWDIO" pad="D3"/>
<connect gate="A" pin="UART0_RX/I2C0_SCL/GPI0_CSN/GP1" pad="2"/>
<connect gate="A" pin="UART0_RX/I2C0_SCL/SPI0_CSN/GP17" pad="22"/>
<connect gate="A" pin="UART0_RX/I2C0_SCL/SPI1_CSN/GP13" pad="17"/>
<connect gate="A" pin="UART0_TX/I2C0_SDA/GPI1_RX/GP12" pad="16"/>
<connect gate="A" pin="UART0_TX/I2C0_SDA/SPI0_RX/GP0" pad="1"/>
<connect gate="A" pin="UART0_TX/I2C0_SDA/SPI0_RX/GP16" pad="21"/>
<connect gate="A" pin="UART1_RX/I2C0_SCL/SPI0_CSN/GP5" pad="7"/>
<connect gate="A" pin="UART1_RX/I2C0_SCL/SPI1_CSN/GP9" pad="12"/>
<connect gate="A" pin="UART1_TX/I2C0_SDA/SPI0_RX/GP4" pad="6"/>
<connect gate="A" pin="UART1_TX/I2C0_SDA/SPI1_RX/GP8" pad="11"/>
<connect gate="A" pin="USB_DM" pad="TP2"/>
<connect gate="A" pin="USB_DP" pad="TP3"/>
<connect gate="A" pin="VBUS" pad="40"/>
<connect gate="A" pin="VSYS" pad="39"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2022 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="2648-SC0915TR-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="2648-SC0915CT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_3" value="2648-SC0915DKR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SC0915" constant="no"/>
<attribute name="MFR_NAME" value="Raspberry Pi" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Power_Symbols">
<description>&lt;B&gt;Supply &amp; Ground symbols</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:16502380/2">
<description>Ground (GND) Arrow</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="0" y="-2.54" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="5V0" urn="urn:adsk.eagle:symbol:18498238/2">
<description>5 Volt (5V0) Bar</description>
<wire x1="1.905" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="94"/>
<text x="-0.127" y="3.048" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="5V0" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="12V" urn="urn:adsk.eagle:symbol:18498240/2">
<description>12 Volt (12V) Bar</description>
<wire x1="1.905" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="94"/>
<text x="-0.127" y="3.048" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="12V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:16502425/4" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt; - Ground (GND) Arrow</description>
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="GND" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5V0" urn="urn:adsk.eagle:component:16502399/6" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;  5 Volt (5V0) Bar</description>
<gates>
<gate name="G$1" symbol="5V0" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="5V0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="12V" urn="urn:adsk.eagle:component:16502428/7" prefix="SUPPLY" uservalue="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt; 12 Volt (12V) Bar</description>
<gates>
<gate name="G$1" symbol="12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="12V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jst-ph">
<packages>
<package name="JST-PH4">
<wire x1="-4.95" y1="-1.7" x2="-1.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.95" y1="-1.7" x2="1.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="1.95" y1="-1.7" x2="4.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.8" x2="4.95" y2="2.8" width="0.127" layer="21"/>
<wire x1="4.95" y1="2.8" x2="4.95" y2="0.6" width="0.127" layer="21"/>
<wire x1="4.95" y1="0.6" x2="4.95" y2="-0.3" width="0.127" layer="21"/>
<wire x1="4.95" y1="-0.3" x2="4.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.8" x2="-4.95" y2="0.6" width="0.127" layer="21"/>
<wire x1="-4.95" y1="0.6" x2="-4.95" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-4.95" y1="-0.3" x2="-4.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-4.35" y1="2.2" x2="4.35" y2="2.2" width="0.127" layer="21"/>
<wire x1="-4.35" y1="2.2" x2="-4.35" y2="0.6" width="0.127" layer="21"/>
<wire x1="-4.35" y1="0.6" x2="-4.35" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-4.35" y1="-0.3" x2="-4.35" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-4.35" y1="-1.1" x2="-1.95" y2="-1.1" width="0.127" layer="21"/>
<wire x1="4.35" y1="2.2" x2="4.35" y2="0.6" width="0.127" layer="21"/>
<wire x1="4.35" y1="0.6" x2="4.35" y2="-0.3" width="0.127" layer="21"/>
<wire x1="4.35" y1="-0.3" x2="4.35" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-4.95" y1="-0.3" x2="-4.35" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-4.95" y1="0.6" x2="-4.35" y2="0.6" width="0.127" layer="21"/>
<wire x1="4.35" y1="0.6" x2="4.95" y2="0.6" width="0.127" layer="21"/>
<wire x1="4.35" y1="-0.3" x2="4.95" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-1.95" y1="-1.1" x2="-1.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="1.95" y1="-1.1" x2="1.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="1.95" y1="-1.1" x2="4.35" y2="-1.1" width="0.127" layer="21"/>
<pad name="4" x="-3" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-1" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="1" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="1" x="3" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-5" y="3.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="2.9001" y="-3.4999" size="1.27" layer="21">1</text>
</package>
<package name="JST-PH2">
<wire x1="-2.95" y1="-1.7" x2="-0.4" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-1.7" x2="0.4" y2="-1.7" width="0.127" layer="21"/>
<wire x1="0.4" y1="-1.7" x2="2.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.95" y1="2.8" x2="2.95" y2="2.8" width="0.127" layer="21"/>
<wire x1="2.95" y1="2.8" x2="2.95" y2="0.6" width="0.127" layer="21"/>
<wire x1="2.95" y1="0.6" x2="2.95" y2="-0.3" width="0.127" layer="21"/>
<wire x1="2.95" y1="-0.3" x2="2.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.95" y1="2.8" x2="-2.95" y2="0.6" width="0.127" layer="21"/>
<wire x1="-2.95" y1="0.6" x2="-2.95" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-2.95" y1="-0.3" x2="-2.95" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.35" y1="2.2" x2="2.35" y2="2.2" width="0.127" layer="21"/>
<wire x1="-2.35" y1="2.2" x2="-2.35" y2="0.6" width="0.127" layer="21"/>
<wire x1="-2.35" y1="0.6" x2="-2.35" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-2.35" y1="-0.3" x2="-2.35" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.35" y1="-1.1" x2="-0.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.35" y1="2.2" x2="2.35" y2="0.6" width="0.127" layer="21"/>
<wire x1="2.35" y1="0.6" x2="2.35" y2="-0.3" width="0.127" layer="21"/>
<wire x1="2.35" y1="-0.3" x2="2.35" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.95" y1="-0.3" x2="-2.35" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-2.95" y1="0.6" x2="-2.35" y2="0.6" width="0.127" layer="21"/>
<wire x1="2.35" y1="0.6" x2="2.95" y2="0.6" width="0.127" layer="21"/>
<wire x1="2.35" y1="-0.3" x2="2.95" y2="-0.3" width="0.127" layer="21"/>
<wire x1="0.4" y1="-1.1" x2="2.35" y2="-1.1" width="0.127" layer="21"/>
<wire x1="0.4" y1="-1.1" x2="0.4" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-1.1" x2="-0.4" y2="-1.7" width="0.127" layer="21"/>
<pad name="2" x="-1" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="1" x="1" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-3" y="3.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="0.9001" y="-3.4999" size="1.27" layer="21">1</text>
</package>
</packages>
<symbols>
<symbol name="CON04">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="5.08" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-9.906" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="4" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CON02">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-7.366" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST-PH4" prefix="P" uservalue="yes">
<gates>
<gate name="A" symbol="CON04" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-PH4">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST-PH2" prefix="P" uservalue="yes">
<gates>
<gate name="G$1" symbol="CON02" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-PH2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_Power-Management">
<description>&lt;B&gt;Voltage Regulators, Drivers, Switching controllers</description>
<packages>
<package name="DIP762W53P254L1980H508Q16B" urn="urn:adsk.eagle:footprint:30733592/1">
<description>16-DIP, 2.54 mm (0.10 in) pitch, 7.62 mm (0.30 in) span, 19.80 X 6.35 X 5.08 mm body
 &lt;p&gt;16-pin DIP package with 2.54 mm (0.10 in) pitch, 7.62 mm (0.30 in) span with body size 19.80 X 6.35 X 5.08 mm&lt;/p&gt;</description>
<pad name="1" x="-3.81" y="8.89" drill="0.7887" diameter="1.3887"/>
<pad name="2" x="-3.81" y="6.35" drill="0.7887" diameter="1.3887"/>
<pad name="3" x="-3.81" y="3.81" drill="0.7887" diameter="1.3887"/>
<pad name="4" x="-3.81" y="1.27" drill="0.7887" diameter="1.3887"/>
<pad name="5" x="-3.81" y="-1.27" drill="0.7887" diameter="1.3887"/>
<pad name="6" x="-3.81" y="-3.81" drill="0.7887" diameter="1.3887"/>
<pad name="7" x="-3.81" y="-6.35" drill="0.7887" diameter="1.3887"/>
<pad name="8" x="-3.81" y="-8.89" drill="0.7887" diameter="1.3887"/>
<pad name="9" x="3.81" y="-8.89" drill="0.7887" diameter="1.3887"/>
<pad name="10" x="3.81" y="-6.35" drill="0.7887" diameter="1.3887"/>
<pad name="11" x="3.81" y="-3.81" drill="0.7887" diameter="1.3887"/>
<pad name="12" x="3.81" y="-1.27" drill="0.7887" diameter="1.3887"/>
<pad name="13" x="3.81" y="1.27" drill="0.7887" diameter="1.3887"/>
<pad name="14" x="3.81" y="3.81" drill="0.7887" diameter="1.3887"/>
<pad name="15" x="3.81" y="6.35" drill="0.7887" diameter="1.3887"/>
<pad name="16" x="3.81" y="8.89" drill="0.7887" diameter="1.3887"/>
<circle x="-5.0084" y="8.89" radius="0.25" width="0" layer="21"/>
<wire x1="-3.3" y1="9.8384" x2="-3.3" y2="9.9" width="0.12" layer="21"/>
<wire x1="-3.3" y1="9.9" x2="3.3" y2="9.9" width="0.12" layer="21"/>
<wire x1="3.3" y1="9.9" x2="3.3" y2="9.8384" width="0.12" layer="21"/>
<wire x1="-3.3" y1="-9.8384" x2="-3.3" y2="-9.9" width="0.12" layer="21"/>
<wire x1="-3.3" y1="-9.9" x2="3.3" y2="-9.9" width="0.12" layer="21"/>
<wire x1="3.3" y1="-9.9" x2="3.3" y2="-9.8384" width="0.12" layer="21"/>
<wire x1="-3.3" y1="-9.9" x2="-3.3" y2="9.9" width="0.12" layer="51"/>
<wire x1="-3.3" y1="9.9" x2="3.3" y2="9.9" width="0.12" layer="51"/>
<wire x1="3.3" y1="9.9" x2="3.3" y2="-9.9" width="0.12" layer="51"/>
<wire x1="3.3" y1="-9.9" x2="-3.3" y2="-9.9" width="0.12" layer="51"/>
<text x="0" y="10.535" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.535" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="DIP762W53P254L1980H508Q16B" urn="urn:adsk.eagle:package:30733598/1" type="model">
<description>16-DIP, 2.54 mm (0.10 in) pitch, 7.62 mm (0.30 in) span, 19.80 X 6.35 X 5.08 mm body
 &lt;p&gt;16-pin DIP package with 2.54 mm (0.10 in) pitch, 7.62 mm (0.30 in) span with body size 19.80 X 6.35 X 5.08 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIP762W53P254L1980H508Q16B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SN754410" urn="urn:adsk.eagle:symbol:30733581/1">
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<pin name="1,2EN" x="-15.24" y="5.08" length="short" direction="in"/>
<pin name="1A" x="-15.24" y="2.54" length="short" direction="in"/>
<pin name="2A" x="-15.24" y="0" length="short" direction="in"/>
<pin name="3,4EN" x="-15.24" y="-2.54" length="short" direction="in"/>
<pin name="3A" x="-15.24" y="-5.08" length="short" direction="in"/>
<pin name="4A" x="-15.24" y="-7.62" length="short" direction="in"/>
<pin name="VCC2" x="-15.24" y="10.16" length="short" direction="pwr"/>
<pin name="VCC1" x="-15.24" y="12.7" length="short" direction="pwr"/>
<pin name="1Y" x="15.24" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="2Y" x="15.24" y="5.08" length="short" direction="out" rot="R180"/>
<pin name="3Y" x="15.24" y="0" length="short" direction="out" rot="R180"/>
<pin name="4Y" x="15.24" y="-5.08" length="short" direction="out" rot="R180"/>
<pin name="GND1" x="-15.24" y="-12.7" length="short" direction="pwr"/>
<pin name="GND2" x="-15.24" y="-15.24" length="short" direction="pwr"/>
<pin name="GND3" x="15.24" y="-12.7" length="short" direction="pwr" rot="R180"/>
<pin name="GND4" x="15.24" y="-15.24" length="short" direction="pwr" rot="R180"/>
<text x="0" y="17.78" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-20.32" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SN754410" urn="urn:adsk.eagle:component:30733623/1" prefix="IC">
<description>Quadruple Half-H Driver&lt;br&gt;

&lt;a href="https://www.ti.com/lit/ds/symlink/sn754410.pdf?ts=1613019217967&amp;ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FSN754410%253Fqgpn%253Dsn754410"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SN754410" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIP762W53P254L1980H508Q16B">
<connects>
<connect gate="G$1" pin="1,2EN" pad="1"/>
<connect gate="G$1" pin="1A" pad="2"/>
<connect gate="G$1" pin="1Y" pad="3"/>
<connect gate="G$1" pin="2A" pad="7"/>
<connect gate="G$1" pin="2Y" pad="6"/>
<connect gate="G$1" pin="3,4EN" pad="9"/>
<connect gate="G$1" pin="3A" pad="10"/>
<connect gate="G$1" pin="3Y" pad="11"/>
<connect gate="G$1" pin="4A" pad="15"/>
<connect gate="G$1" pin="4Y" pad="14"/>
<connect gate="G$1" pin="GND1" pad="4"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="GND3" pad="12"/>
<connect gate="G$1" pin="GND4" pad="13"/>
<connect gate="G$1" pin="VCC1" pad="16"/>
<connect gate="G$1" pin="VCC2" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30733598/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="IC_Power-Management" constant="no"/>
<attribute name="DESCRIPTION" value="Quadruple Half-H Driver" constant="no"/>
<attribute name="MANUFACTURER" value="TEXAS INSTRUMENTS" constant="no"/>
<attribute name="MPN" value="SN754410" constant="no"/>
<attribute name="OPERATING_TEMP" value="-40°C to 150°C (TJ)" constant="no"/>
<attribute name="PART_STATUS" value="Active" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="ROHS3 Compliant" constant="no"/>
<attribute name="SERIES" value="SN754" constant="no"/>
<attribute name="SUB_CATEGORY" value="Motor Driver" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X02" urn="urn:adsk.eagle:footprint:22309/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90" urn="urn:adsk.eagle:footprint:22310/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:22435/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X02"/>
</packageinstances>
</package3d>
<package3d name="1X02/90" urn="urn:adsk.eagle:package:22437/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X02/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD2" urn="urn:adsk.eagle:symbol:22308/1" library_version="4">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X2" urn="urn:adsk.eagle:component:22516/4" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22435/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="98" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22437/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="24" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ON Semiconductor - 2N7000-D26Z">
<description>Upverter Parts Library

Created by Upverter.com</description>
<packages>
<package name="ON_SEMICONDUCTOR_2N7000-D26Z_0">
<description>Transistor Mosfet N-ch 60V 0.2A 3-PIN TO-92 T/r</description>
<wire x1="-2.095" y1="-2.6" x2="-2.095" y2="2.6" width="0.15" layer="51"/>
<wire x1="-2.095" y1="2.6" x2="2.095" y2="2.6" width="0.15" layer="51"/>
<wire x1="2.095" y1="2.6" x2="2.095" y2="-2.6" width="0.15" layer="51"/>
<wire x1="2.095" y1="-2.6" x2="-2.095" y2="-2.6" width="0.15" layer="51"/>
<wire x1="-2.095" y1="2.6" x2="-1.48" y2="2.6" width="0.15" layer="21"/>
<wire x1="-2.095" y1="-2.6" x2="-1.48" y2="-2.6" width="0.15" layer="21"/>
<wire x1="-2.095" y1="-2.6" x2="-2.095" y2="2.6" width="0.15" layer="21"/>
<wire x1="0.72" y1="-2.6" x2="0.72" y2="2.6" width="0.15" layer="21" curve="111.257327"/>
<text x="-2.345" y="3.5" size="1" layer="25">&gt;NAME</text>
<pad name="1" x="-0.38" y="2.6" drill="0.95" diameter="1.45" shape="square"/>
<pad name="2" x="-0.38" y="0" drill="0.95" diameter="1.45"/>
<pad name="3" x="-0.38" y="-2.6" drill="0.95" diameter="1.45"/>
</package>
</packages>
<symbols>
<symbol name="ON_SEMICONDUCTOR_2N7000-D26Z_0_0">
<description>Transistor Mosfet N-ch 60V 0.2A 3-PIN TO-92 T/r</description>
<wire x1="3.556" y1="-7.62" x2="3.556" y2="-2.54" width="0.508" layer="94"/>
<wire x1="4.318" y1="-3.556" x2="4.318" y2="-2.54" width="0.508" layer="94"/>
<wire x1="4.318" y1="-7.112" x2="7.62" y2="-7.112" width="0.508" layer="94"/>
<wire x1="4.318" y1="-3.048" x2="7.62" y2="-3.048" width="0.508" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.508" layer="94"/>
<wire x1="4.826" y1="-5.08" x2="6.096" y2="-4.318" width="0.508" layer="94"/>
<wire x1="4.826" y1="-5.08" x2="6.096" y2="-5.842" width="0.508" layer="94"/>
<wire x1="6.096" y1="-4.318" x2="6.096" y2="-5.842" width="0.508" layer="94"/>
<wire x1="5.842" y1="-4.826" x2="5.842" y2="-5.588" width="0.508" layer="94"/>
<wire x1="4.318" y1="-6.604" x2="4.318" y2="-7.62" width="0.508" layer="94"/>
<wire x1="4.318" y1="-4.572" x2="4.318" y2="-5.588" width="0.508" layer="94"/>
<wire x1="7.62" y1="-7.112" x2="9.398" y2="-7.112" width="0.508" layer="94"/>
<wire x1="9.398" y1="-7.112" x2="9.398" y2="-3.048" width="0.508" layer="94"/>
<wire x1="9.398" y1="-3.048" x2="7.62" y2="-3.048" width="0.508" layer="94"/>
<wire x1="9.398" y1="-4.572" x2="8.89" y2="-5.588" width="0.508" layer="94"/>
<wire x1="8.89" y1="-5.588" x2="9.906" y2="-5.588" width="0.508" layer="94"/>
<wire x1="9.906" y1="-5.588" x2="9.398" y2="-4.572" width="0.508" layer="94"/>
<wire x1="9.398" y1="-4.572" x2="9.906" y2="-4.572" width="0.508" layer="94"/>
<wire x1="9.398" y1="-4.572" x2="8.89" y2="-4.572" width="0.508" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="3.556" y2="-7.62" width="0.15" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.15" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-3.048" width="0.15" layer="94"/>
<text x="10.922" y="-2.54" size="2.54" layer="95" align="top-left">&gt;NAME</text>
<text x="10.922" y="-5.334" size="2.54" layer="95" align="top-left">2N7000-D26Z</text>
<pin name="G" x="-2.54" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="S" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="D" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R270"/>
<circle x="7.62" y="-3.048" radius="0.254" width="0.254" layer="94"/>
<circle x="7.62" y="-7.112" radius="0.254" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ON_SEMICONDUCTOR_2N7000-D26Z" prefix="Q">
<description>Transistor Mosfet N-ch 60V 0.2A 3-PIN TO-92 T/r</description>
<gates>
<gate name="G$0" symbol="ON_SEMICONDUCTOR_2N7000-D26Z_0_0" x="0" y="0"/>
</gates>
<devices>
<device name="ON_SEMICONDUCTOR_2N7000-D26Z_0_0" package="ON_SEMICONDUCTOR_2N7000-D26Z_0">
<connects>
<connect gate="G$0" pin="D" pad="3"/>
<connect gate="G$0" pin="G" pad="2"/>
<connect gate="G$0" pin="S" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="AUTOMOTIVE" value="No"/>
<attribute name="CENTROID_NOT_SPECIFIED" value="No"/>
<attribute name="CONTINUOUS_DRAIN_CURRENT" value="200mA"/>
<attribute name="DATASHEET" value="https://www.onsemi.com/pdf/datasheet/nds7002a-d.pdf"/>
<attribute name="DEPLETION_MODE" value="False"/>
<attribute name="DEVICE_CLASS_L1" value="Discrete Semiconductors"/>
<attribute name="DEVICE_CLASS_L2" value="Transistors"/>
<attribute name="DEVICE_CLASS_L3" value="MOSFETs"/>
<attribute name="DIGIKEY_DESCRIPTION" value="MOSFET N-CH 60V 200MA TO92-3"/>
<attribute name="DIGIKEY_PART_NUMBER" value="2N7000-D26ZCT-ND"/>
<attribute name="DRAIN_TO_SOURCE_BREAKDOWN_VOLTAGE" value="60V"/>
<attribute name="DRAIN_TO_SOURCE_RESISTANCE" value="1.2Ω"/>
<attribute name="DRAIN_TO_SOURCE_VOLTAGE" value="60V"/>
<attribute name="FOOTPRINT_VARIANT_NAME_0" value="Recommended_Land_Pattern"/>
<attribute name="GATE_TO_SOURCE_VOLTAGE" value="20V"/>
<attribute name="HEIGHT" value="8.77mm"/>
<attribute name="INPUT_CAPACITACE_AT_VDS" value="20pF @ 25V"/>
<attribute name="LEAD_FREE" value="Yes"/>
<attribute name="MAX_FORWARD_DIODE_VOLTAGE" value="1.5V"/>
<attribute name="MAX_JUNCTION_TEMP" value="+150°C"/>
<attribute name="MF" value="ON Semiconductor"/>
<attribute name="MFG_PACKAGE_IDENT" value="135AR"/>
<attribute name="MFG_PACKAGE_IDENT_COMPONENT_ID" value="9652051e5763c9af"/>
<attribute name="MFG_PACKAGE_IDENT_DATE" value="09/2016"/>
<attribute name="MFG_PACKAGE_IDENT_REV" value="O"/>
<attribute name="MOUSER_DESCRIPTION" value="N-Channel 60 V 5 Ohm Enhancement Mode Field Effect Transistor-TO-92"/>
<attribute name="MOUSER_PART_NUMBER" value="512-2N7000D26Z"/>
<attribute name="MPN" value="2N7000-D26Z"/>
<attribute name="NUMBER_OF_CHANNELS" value="1"/>
<attribute name="NUMBER_OF_N_CHANNELS" value="1"/>
<attribute name="PACKAGE" value="TO92"/>
<attribute name="POWER_DISSIPATION" value="400mW"/>
<attribute name="PREFIX" value="Q"/>
<attribute name="PULSE_DRAIN_CURRENT" value="500mA"/>
<attribute name="ROHS" value="Yes"/>
<attribute name="RTHJA_MAX" value="312.5°C/W"/>
<attribute name="STANDOFF_HEIGHT" value="2.54mm"/>
<attribute name="TEMPERATURE_RANGE_HIGH" value="+150°C"/>
<attribute name="TEMPERATURE_RANGE_LOW" value="-55°C"/>
<attribute name="THRESHOLD_VGS_MAX" value="3V"/>
<attribute name="THRESHOLD_VGS_MIN" value="0.8V"/>
<attribute name="TRANSCONDUCTANCE" value="0.32S"/>
<attribute name="TURN_OFF_DELAY_TIME" value="10ns"/>
<attribute name="TURN_ON_DELAY_TIME" value="10ns"/>
<attribute name="TYP_FORWARD_DIODE_VOLTAGE" value="0.88V"/>
<attribute name="VERIFICATION_VERSION" value="0.0.0.3"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="rpi-pico" deviceset="SC0915" device=""/>
<part name="SUPPLY2" library="Power_Symbols" deviceset="GND" device="" value="GND"/>
<part name="SUPPLY3" library="Power_Symbols" deviceset="GND" device="" value="GND"/>
<part name="SUPPLY1" library="Power_Symbols" deviceset="5V0" device="" value="5V0"/>
<part name="P1" library="jst-ph" deviceset="JST-PH4" device=""/>
<part name="IC1" library="IC_Power-Management" deviceset="SN754410" device="" package3d_urn="urn:adsk.eagle:package:30733598/1"/>
<part name="P2" library="jst-ph" deviceset="JST-PH4" device=""/>
<part name="SUPPLY4" library="Power_Symbols" deviceset="12V" device="" value="12V"/>
<part name="P3" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="SUPPLY5" library="Power_Symbols" deviceset="GND" device="" value="GND"/>
<part name="P4" library="jst-ph" deviceset="JST-PH4" device=""/>
<part name="P5" library="jst-ph" deviceset="JST-PH4" device=""/>
<part name="P6" library="jst-ph" deviceset="JST-PH4" device=""/>
<part name="P7" library="jst-ph" deviceset="JST-PH4" device=""/>
<part name="DEBUG" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X2" device="" package3d_urn="urn:adsk.eagle:package:22435/2"/>
<part name="Q1" library="ON Semiconductor - 2N7000-D26Z" deviceset="ON_SEMICONDUCTOR_2N7000-D26Z" device="ON_SEMICONDUCTOR_2N7000-D26Z_0_0"/>
<part name="Q2" library="ON Semiconductor - 2N7000-D26Z" deviceset="ON_SEMICONDUCTOR_2N7000-D26Z" device="ON_SEMICONDUCTOR_2N7000-D26Z_0_0"/>
<part name="Q3" library="ON Semiconductor - 2N7000-D26Z" deviceset="ON_SEMICONDUCTOR_2N7000-D26Z" device="ON_SEMICONDUCTOR_2N7000-D26Z_0_0"/>
<part name="Q4" library="ON Semiconductor - 2N7000-D26Z" deviceset="ON_SEMICONDUCTOR_2N7000-D26Z" device="ON_SEMICONDUCTOR_2N7000-D26Z_0_0"/>
<part name="SUPPLY6" library="Power_Symbols" deviceset="GND" device="" value="GND"/>
<part name="P8" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P9" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P10" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P11" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P12" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P13" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P14" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P15" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P16" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P17" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P18" library="jst-ph" deviceset="JST-PH2" device=""/>
<part name="P19" library="jst-ph" deviceset="JST-PH2" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-53.34" y="58.42" size="1.778" layer="91">JST I2C0 STEMMA QT</text>
<text x="60.96" y="53.34" size="1.778" layer="91">Raspberry Pi Pico</text>
<text x="-109.22" y="-15.24" size="1.778" layer="91">Laser driver</text>
<text x="-43.18" y="50.8" size="1.778" layer="91">Integrated stepper driver</text>
<text x="-33.02" y="-10.16" size="1.778" layer="91">12V Power</text>
<text x="-96.52" y="10.16" size="1.778" layer="91">Stepper connect</text>
<text x="-33.02" y="99.06" size="1.778" layer="91" rot="R180">JST I2C1 STEMMA QT</text>
<text x="-2.54" y="99.06" size="1.778" layer="91">DEBUG jumper pins</text>
</plain>
<instances>
<instance part="U1" gate="A" x="5.08" y="83.82" smashed="yes">
<attribute name="NAME" x="63.8556" y="92.9386" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="63.2206" y="90.3986" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="132.08" y="88.9" smashed="yes" rot="R180">
<attribute name="VALUE" x="132.08" y="91.44" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="5.08" y="10.16" smashed="yes">
<attribute name="VALUE" x="5.08" y="7.62" size="1.778" layer="96" align="center"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="139.7" y="53.34" smashed="yes">
<attribute name="VALUE" x="139.573" y="56.388" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="P1" gate="A" x="-50.8" y="68.58" smashed="yes" rot="MR180">
<attribute name="NAME" x="-53.34" y="62.738" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-53.34" y="78.486" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="IC1" gate="G$1" x="-30.48" y="25.4" smashed="yes" rot="R180">
<attribute name="NAME" x="-30.48" y="7.62" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="-30.48" y="45.72" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="P2" gate="A" x="-71.12" y="20.32" smashed="yes" rot="MR180">
<attribute name="NAME" x="-73.66" y="14.478" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-73.66" y="30.226" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="-12.7" y="-15.24" smashed="yes" rot="R180">
<attribute name="VALUE" x="-12.573" y="-18.288" size="1.778" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="P3" gate="G$1" x="-30.48" y="-2.54" smashed="yes">
<attribute name="NAME" x="-33.02" y="0.762" size="1.778" layer="95"/>
<attribute name="VALUE" x="-33.02" y="-9.906" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="-20.32" y="2.54" smashed="yes" rot="R180">
<attribute name="VALUE" x="-20.32" y="5.08" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="P4" gate="A" x="-83.82" y="20.32" smashed="yes" rot="MR180">
<attribute name="NAME" x="-86.36" y="14.478" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-86.36" y="30.226" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="P5" gate="A" x="-96.52" y="20.32" smashed="yes" rot="MR180">
<attribute name="NAME" x="-99.06" y="14.478" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-99.06" y="30.226" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="P6" gate="A" x="-109.22" y="20.32" smashed="yes" rot="MR180">
<attribute name="NAME" x="-111.76" y="14.478" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-111.76" y="30.226" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="P7" gate="A" x="-50.8" y="88.9" smashed="yes" rot="MR180">
<attribute name="NAME" x="-53.34" y="83.058" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-53.34" y="98.806" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="DEBUG" gate="G$1" x="5.08" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="-0.635" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.16" y="90.17" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="Q1" gate="G$0" x="-111.76" y="-20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="-114.3" y="-31.242" size="2.54" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="Q2" gate="G$0" x="-91.44" y="-20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="-93.98" y="-31.242" size="2.54" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="Q3" gate="G$0" x="-71.12" y="-20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="-73.66" y="-31.242" size="2.54" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="Q4" gate="G$0" x="-50.8" y="-20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="-53.34" y="-31.242" size="2.54" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="-129.54" y="-38.1" smashed="yes">
<attribute name="VALUE" x="-129.54" y="-40.64" size="1.778" layer="96" align="center"/>
</instance>
<instance part="P8" gate="G$1" x="-109.22" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="-112.522" y="-48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-101.854" y="-48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P9" gate="G$1" x="-109.22" y="-58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-112.522" y="-60.96" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-101.854" y="-60.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P10" gate="G$1" x="-109.22" y="-71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="-112.522" y="-73.66" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-101.854" y="-73.66" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P11" gate="G$1" x="-88.9" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="-92.202" y="-48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-81.534" y="-48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P12" gate="G$1" x="-88.9" y="-58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-92.202" y="-60.96" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-81.534" y="-60.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P13" gate="G$1" x="-88.9" y="-71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="-92.202" y="-73.66" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-81.534" y="-73.66" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P14" gate="G$1" x="-68.58" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="-71.882" y="-48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-61.214" y="-48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P15" gate="G$1" x="-68.58" y="-58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-71.882" y="-60.96" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-61.214" y="-60.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P16" gate="G$1" x="-68.58" y="-71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="-71.882" y="-73.66" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-61.214" y="-73.66" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P17" gate="G$1" x="-48.26" y="-45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="-51.562" y="-48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-40.894" y="-48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P18" gate="G$1" x="-48.26" y="-58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-51.562" y="-60.96" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-40.894" y="-60.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P19" gate="G$1" x="-48.26" y="-71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="-51.562" y="-73.66" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-40.894" y="-73.66" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY2" gate="G$1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_7"/>
<wire x1="129.54" y1="20.32" x2="132.08" y2="20.32" width="0.1524" layer="91"/>
<wire x1="132.08" y1="20.32" x2="132.08" y2="33.02" width="0.1524" layer="91"/>
<wire x1="132.08" y1="33.02" x2="132.08" y2="45.72" width="0.1524" layer="91"/>
<wire x1="132.08" y1="45.72" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
<wire x1="132.08" y1="55.88" x2="132.08" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND_8"/>
<wire x1="129.54" y1="45.72" x2="132.08" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND_9"/>
<wire x1="129.54" y1="55.88" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="AGND/GND"/>
<wire x1="129.54" y1="33.02" x2="132.08" y2="33.02" width="0.1524" layer="91"/>
<junction x="132.08" y="33.02"/>
<junction x="132.08" y="45.72"/>
<junction x="132.08" y="55.88"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="GND_2"/>
<wire x1="7.62" y1="78.74" x2="5.08" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="G$1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_3"/>
<wire x1="5.08" y1="78.74" x2="5.08" y2="71.12" width="0.1524" layer="91"/>
<wire x1="5.08" y1="71.12" x2="5.08" y2="66.04" width="0.1524" layer="91"/>
<wire x1="5.08" y1="66.04" x2="5.08" y2="53.34" width="0.1524" layer="91"/>
<wire x1="5.08" y1="53.34" x2="5.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="5.08" y1="40.64" x2="5.08" y2="27.94" width="0.1524" layer="91"/>
<wire x1="5.08" y1="27.94" x2="5.08" y2="12.7" width="0.1524" layer="91"/>
<wire x1="7.62" y1="66.04" x2="5.08" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND_4"/>
<wire x1="7.62" y1="53.34" x2="5.08" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND_5"/>
<wire x1="7.62" y1="40.64" x2="5.08" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND_6"/>
<wire x1="7.62" y1="27.94" x2="5.08" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND2"/>
<wire x1="-15.24" y1="40.64" x2="-12.7" y2="40.64" width="0.1524" layer="91"/>
<junction x="5.08" y="66.04"/>
<junction x="5.08" y="53.34"/>
<junction x="5.08" y="40.64"/>
<junction x="5.08" y="27.94"/>
<pinref part="IC1" gate="G$1" pin="GND1"/>
<wire x1="-12.7" y1="40.64" x2="5.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="38.1" x2="-12.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="38.1" x2="-12.7" y2="40.64" width="0.1524" layer="91"/>
<junction x="-12.7" y="40.64"/>
<wire x1="-12.7" y1="40.64" x2="-12.7" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="48.26" x2="-48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="48.26" x2="-48.26" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="40.64" x2="-48.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="38.1" x2="-45.72" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND3"/>
<pinref part="IC1" gate="G$1" pin="GND4"/>
<wire x1="-45.72" y1="40.64" x2="-48.26" y2="40.64" width="0.1524" layer="91"/>
<junction x="-48.26" y="40.64"/>
<pinref part="DEBUG" gate="G$1" pin="2"/>
<wire x1="5.08" y1="93.98" x2="5.08" y2="78.74" width="0.1524" layer="91"/>
<junction x="5.08" y="78.74"/>
<pinref part="P7" gate="A" pin="4"/>
<wire x1="-12.7" y1="93.98" x2="-43.18" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="73.66" x2="-12.7" y2="93.98" width="0.1524" layer="91"/>
<pinref part="P1" gate="A" pin="4"/>
<wire x1="-43.18" y1="73.66" x2="-12.7" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="73.66" x2="-12.7" y2="71.12" width="0.1524" layer="91"/>
<junction x="-12.7" y="73.66"/>
<wire x1="-12.7" y1="71.12" x2="5.08" y2="71.12" width="0.1524" layer="91"/>
<junction x="5.08" y="71.12"/>
</segment>
<segment>
<pinref part="P3" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="-2.54" x2="-20.32" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-2.54" x2="-20.32" y2="0" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="Q1" gate="G$0" pin="S"/>
<wire x1="-121.92" y1="-27.94" x2="-129.54" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="G$1" pin="GND"/>
<wire x1="-129.54" y1="-27.94" x2="-129.54" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$0" pin="S"/>
<wire x1="-129.54" y1="-33.02" x2="-129.54" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="-27.94" x2="-104.14" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-27.94" x2="-104.14" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-33.02" x2="-129.54" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-129.54" y="-33.02"/>
<pinref part="Q3" gate="G$0" pin="S"/>
<wire x1="-81.28" y1="-27.94" x2="-83.82" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-27.94" x2="-83.82" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-33.02" x2="-104.14" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-104.14" y="-33.02"/>
<pinref part="Q4" gate="G$0" pin="S"/>
<wire x1="-60.96" y1="-27.94" x2="-63.5" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-27.94" x2="-63.5" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-33.02" x2="-83.82" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-83.82" y="-33.02"/>
</segment>
</net>
<net name="5V0" class="0">
<segment>
<pinref part="SUPPLY1" gate="G$1" pin="5V0"/>
<wire x1="139.7" y1="53.34" x2="139.7" y2="50.8" width="0.1524" layer="91"/>
<wire x1="139.7" y1="50.8" x2="139.7" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-10.16" x2="-10.16" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="12.7" x2="-15.24" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VCC1"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="20.32" x2="-15.24" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="1,2EN"/>
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="27.94" x2="-15.24" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="3,4EN"/>
<junction x="-10.16" y="12.7"/>
<junction x="-10.16" y="20.32"/>
<pinref part="U1" gate="A" pin="VBUS"/>
<wire x1="129.54" y1="50.8" x2="139.7" y2="50.8" width="0.1524" layer="91"/>
<junction x="139.7" y="50.8"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="4Y"/>
<wire x1="-45.72" y1="30.48" x2="-55.88" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="30.48" x2="-55.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="25.4" x2="-63.5" y2="25.4" width="0.1524" layer="91"/>
<pinref part="P2" gate="A" pin="4"/>
<wire x1="-63.5" y1="25.4" x2="-76.2" y2="25.4" width="0.1524" layer="91"/>
<junction x="-63.5" y="25.4"/>
<pinref part="P4" gate="A" pin="4"/>
<wire x1="-76.2" y1="25.4" x2="-88.9" y2="25.4" width="0.1524" layer="91"/>
<junction x="-76.2" y="25.4"/>
<pinref part="P5" gate="A" pin="4"/>
<wire x1="-88.9" y1="25.4" x2="-101.6" y2="25.4" width="0.1524" layer="91"/>
<junction x="-88.9" y="25.4"/>
<pinref part="P6" gate="A" pin="4"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="3Y"/>
<wire x1="-45.72" y1="25.4" x2="-53.34" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="25.4" x2="-53.34" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="22.86" x2="-63.5" y2="22.86" width="0.1524" layer="91"/>
<pinref part="P2" gate="A" pin="3"/>
<wire x1="-63.5" y1="22.86" x2="-76.2" y2="22.86" width="0.1524" layer="91"/>
<junction x="-63.5" y="22.86"/>
<pinref part="P4" gate="A" pin="3"/>
<wire x1="-76.2" y1="22.86" x2="-88.9" y2="22.86" width="0.1524" layer="91"/>
<junction x="-76.2" y="22.86"/>
<pinref part="P5" gate="A" pin="3"/>
<wire x1="-88.9" y1="22.86" x2="-101.6" y2="22.86" width="0.1524" layer="91"/>
<junction x="-88.9" y="22.86"/>
<pinref part="P6" gate="A" pin="3"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="2Y"/>
<wire x1="-45.72" y1="20.32" x2="-63.5" y2="20.32" width="0.1524" layer="91"/>
<pinref part="P2" gate="A" pin="2"/>
<wire x1="-63.5" y1="20.32" x2="-76.2" y2="20.32" width="0.1524" layer="91"/>
<junction x="-63.5" y="20.32"/>
<pinref part="P4" gate="A" pin="2"/>
<wire x1="-76.2" y1="20.32" x2="-88.9" y2="20.32" width="0.1524" layer="91"/>
<junction x="-76.2" y="20.32"/>
<pinref part="P5" gate="A" pin="2"/>
<wire x1="-88.9" y1="20.32" x2="-101.6" y2="20.32" width="0.1524" layer="91"/>
<junction x="-88.9" y="20.32"/>
<pinref part="P6" gate="A" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="1Y"/>
<wire x1="-45.72" y1="15.24" x2="-53.34" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="15.24" x2="-53.34" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="17.78" x2="-63.5" y2="17.78" width="0.1524" layer="91"/>
<pinref part="P2" gate="A" pin="1"/>
<wire x1="-63.5" y1="17.78" x2="-76.2" y2="17.78" width="0.1524" layer="91"/>
<junction x="-63.5" y="17.78"/>
<pinref part="P4" gate="A" pin="1"/>
<wire x1="-76.2" y1="17.78" x2="-88.9" y2="17.78" width="0.1524" layer="91"/>
<junction x="-76.2" y="17.78"/>
<pinref part="P5" gate="A" pin="1"/>
<wire x1="-88.9" y1="17.78" x2="-101.6" y2="17.78" width="0.1524" layer="91"/>
<junction x="-88.9" y="17.78"/>
<pinref part="P6" gate="A" pin="1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="1A"/>
<wire x1="-15.24" y1="22.86" x2="-5.08" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="22.86" x2="-5.08" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="38.1" x2="7.62" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="I2C1_SDA/SPI1_SCK/GP14"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="2A"/>
<wire x1="-15.24" y1="25.4" x2="-2.54" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="25.4" x2="-2.54" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="35.56" x2="7.62" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="I2C1_SCL/SPI1_TX/GP15"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="3A"/>
<wire x1="-15.24" y1="30.48" x2="0" y2="30.48" width="0.1524" layer="91"/>
<wire x1="0" y1="30.48" x2="0" y2="33.02" width="0.1524" layer="91"/>
<wire x1="0" y1="33.02" x2="7.62" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="UART0_TX/I2C0_SDA/SPI0_RX/GP16"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="4A"/>
<wire x1="-15.24" y1="33.02" x2="-7.62" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="33.02" x2="-7.62" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="27.94" x2="2.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="2.54" y1="27.94" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="2.54" y1="30.48" x2="7.62" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="UART0_RX/I2C0_SCL/SPI0_CSN/GP17"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VCC2"/>
<wire x1="-15.24" y1="15.24" x2="-12.7" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="G$1" pin="12V"/>
<pinref part="P3" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-5.08" x2="-17.78" y2="-5.08" width="0.1524" layer="91"/>
<junction x="-12.7" y="-5.08"/>
<wire x1="-17.78" y1="-5.08" x2="-12.7" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-5.08" x2="-17.78" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-35.56" x2="-45.72" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-17.78" y="-5.08"/>
<pinref part="P8" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="-35.56" x2="-66.04" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="-35.56" x2="-86.36" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="-35.56" x2="-106.68" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="-35.56" x2="-106.68" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="P11" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="-35.56" x2="-86.36" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-86.36" y="-35.56"/>
<pinref part="P14" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="-38.1" x2="-66.04" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-66.04" y="-35.56"/>
<pinref part="P17" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="-38.1" x2="-45.72" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-45.72" y="-35.56"/>
<pinref part="P9" gate="G$1" pin="2"/>
<wire x1="-106.68" y1="-38.1" x2="-106.68" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-106.68" y="-38.1"/>
<pinref part="P10" gate="G$1" pin="2"/>
<wire x1="-106.68" y1="-50.8" x2="-106.68" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-106.68" y="-50.8"/>
<pinref part="P12" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="-38.1" x2="-86.36" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-86.36" y="-38.1"/>
<pinref part="P13" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="-50.8" x2="-86.36" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-86.36" y="-50.8"/>
<pinref part="P15" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="-38.1" x2="-66.04" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-66.04" y="-38.1"/>
<pinref part="P16" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="-50.8" x2="-66.04" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-66.04" y="-50.8"/>
<pinref part="P18" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="-38.1" x2="-45.72" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-45.72" y="-38.1"/>
<pinref part="P19" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="-50.8" x2="-45.72" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-45.72" y="-50.8"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="-7.62" y1="43.18" x2="-7.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="66.04" x2="-43.18" y2="66.04" width="0.1524" layer="91"/>
<pinref part="P1" gate="A" pin="1"/>
<pinref part="U1" gate="A" pin="UART0_RX/I2C0_SCL/SPI1_CSN/GP13"/>
<wire x1="7.62" y1="43.18" x2="-7.62" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="A" pin="UART0_TX/I2C0_SDA/GPI1_RX/GP12"/>
<wire x1="7.62" y1="45.72" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="45.72" x2="-5.08" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="68.58" x2="-43.18" y2="68.58" width="0.1524" layer="91"/>
<pinref part="P1" gate="A" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="A" pin="3V3(OUT)"/>
<wire x1="129.54" y1="40.64" x2="134.62" y2="40.64" width="0.1524" layer="91"/>
<wire x1="134.62" y1="40.64" x2="134.62" y2="104.14" width="0.1524" layer="91"/>
<wire x1="134.62" y1="104.14" x2="-15.24" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="104.14" x2="-15.24" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="91.44" x2="-15.24" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="71.12" x2="-43.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="P1" gate="A" pin="3"/>
<pinref part="P7" gate="A" pin="3"/>
<wire x1="-43.18" y1="91.44" x2="-15.24" y2="91.44" width="0.1524" layer="91"/>
<junction x="-15.24" y="91.44"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="P7" gate="A" pin="1"/>
<wire x1="-43.18" y1="86.36" x2="-2.54" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="86.36" x2="-2.54" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="I2C1_SCL/SPI1_TX/GP11"/>
<wire x1="-2.54" y1="48.26" x2="7.62" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="P7" gate="A" pin="2"/>
<wire x1="-43.18" y1="88.9" x2="0" y2="88.9" width="0.1524" layer="91"/>
<wire x1="0" y1="88.9" x2="0" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="I2C1_SDA/SPI1_SCK/GP10"/>
<wire x1="0" y1="50.8" x2="7.62" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U1" gate="A" pin="UART1_RX/I2C0_SCL/SPI0_CSN/GP5"/>
<wire x1="7.62" y1="68.58" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
<pinref part="DEBUG" gate="G$1" pin="1"/>
<wire x1="2.54" y1="68.58" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="Q3" gate="G$0" pin="G"/>
<pinref part="Q4" gate="G$0" pin="G"/>
<wire x1="-78.74" y1="-17.78" x2="-58.42" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-78.74" y="-17.78"/>
<pinref part="U1" gate="A" pin="I2C1_SCL/SPI0_TX/GP3"/>
<wire x1="7.62" y1="73.66" x2="-7.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="73.66" x2="-7.62" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="78.74" x2="-116.84" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="78.74" x2="-116.84" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-12.7" x2="-78.74" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="-12.7" x2="-78.74" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="Q1" gate="G$0" pin="D"/>
<pinref part="P8" gate="G$1" pin="1"/>
<wire x1="-109.22" y1="-27.94" x2="-109.22" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="P9" gate="G$1" pin="1"/>
<wire x1="-109.22" y1="-38.1" x2="-109.22" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-109.22" y="-38.1"/>
<pinref part="P10" gate="G$1" pin="1"/>
<wire x1="-109.22" y1="-50.8" x2="-109.22" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-109.22" y="-50.8"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="Q2" gate="G$0" pin="D"/>
<pinref part="P11" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="-27.94" x2="-88.9" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="P12" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="-38.1" x2="-88.9" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-88.9" y="-38.1"/>
<pinref part="P13" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="-50.8" x2="-88.9" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-88.9" y="-50.8"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Q3" gate="G$0" pin="D"/>
<pinref part="P14" gate="G$1" pin="1"/>
<wire x1="-68.58" y1="-27.94" x2="-68.58" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="P15" gate="G$1" pin="1"/>
<wire x1="-68.58" y1="-38.1" x2="-68.58" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-68.58" y="-38.1"/>
<pinref part="P16" gate="G$1" pin="1"/>
<wire x1="-68.58" y1="-50.8" x2="-68.58" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-68.58" y="-50.8"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="Q4" gate="G$0" pin="D"/>
<pinref part="P17" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="-27.94" x2="-48.26" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="P18" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="-38.1" x2="-48.26" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-48.26" y="-38.1"/>
<pinref part="P19" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="-50.8" x2="-48.26" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-48.26" y="-50.8"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="Q2" gate="G$0" pin="G"/>
<pinref part="Q1" gate="G$0" pin="G"/>
<wire x1="-119.38" y1="-17.78" x2="-99.06" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-119.38" y="-17.78"/>
<wire x1="-121.92" y1="-17.78" x2="-119.38" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="81.28" x2="-121.92" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="I2C1_SDA/SPI0_SCK/GP2"/>
<wire x1="7.62" y1="76.2" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="76.2" x2="-5.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="81.28" x2="-121.92" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
